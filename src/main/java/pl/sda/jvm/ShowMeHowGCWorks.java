package pl.sda.jvm;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class ShowMeHowGCWorks {
    public static void main(String[] args) throws InterruptedException {

        while (true) {
            final LinkedList<ThereWillBeManyObjectsOfThatClass> myList = new LinkedList<>();
            System.out.println(LocalDateTime.now());
            Thread.sleep(1000);
            for (int i1 = 100_000; i1 < 1000_000; i1++) {
                String s = String.valueOf(i1);
                ThereWillBeManyObjectsOfThatClass thereWillBeManyObjectsOfThatClass = new ThereWillBeManyObjectsOfThatClass(s);
                myList.add(thereWillBeManyObjectsOfThatClass);
            }

        }
    }
}

class ThereWillBeManyObjectsOfThatClass {
    private String someString;


    public ThereWillBeManyObjectsOfThatClass(String someString) {
        this.someString = someString;
    }
}