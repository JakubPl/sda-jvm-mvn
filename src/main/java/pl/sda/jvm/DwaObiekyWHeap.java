package pl.sda.jvm;

import java.util.LinkedList;
import java.util.List;

public class DwaObiekyWHeap {
    public static void main(String[] args) throws InterruptedException {
        List<MojObiekt> myObjects = new LinkedList<>();
        new Thread(() -> {
            while (true) {
                myObjects.add(new MojObiekt(MojObiektDwa.pole));
                MojObiektDwa.pole += 1;
                try {
                    Thread.sleep(5_000);
                } catch (InterruptedException e) {
                }
                System.out.println(MojObiektDwa.pole);
            }
        }).start();
    }
}

class MojObiektDwa {
    public static int pole = 0;
}

class MojObiekt {
    public MojObiekt(int pole) {
        this.pole = pole;
    }

    private int pole;

    public int getPole() {
        return pole;
    }

    public void setPole(int pole) {
        this.pole = pole;
    }
}